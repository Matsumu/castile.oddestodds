# Castile.ODDESTODDS

Repository for ODDESTODDS.COM Test

# Questions
ODDESTODDS.com needs a website that displays odds in real time to their clients.  They have hired you to provide a proof of concept (POC).  This should not be a full product, but rather a demonstration that you can fulfil their eventual requirements.  

The overall requirements for this POC are two deliverables:  a screen displaying 1X2 odds and a back office screen to edit the odds.  It is very important that the odds update in real-time. Both user interfaces can be console applications as this is a POC.

# Assumptions
-since this is a POC, i assumed that authentication is already taken care of
-however, i tried to handle vakidation at front & back end and also ignore some validations
-Been a POC my test cases are few. i am writing test cases to show my knowledge and expertise in unit testing
-i assume the major aim of this test is to show off partly or basically your skills in writing clean, straight forward, reusable code. however, my project architecture or seperation of concerns seems to make the project complex or bigger. i assumed that this will lead to an enterprise and multi-team development and need should be for an N-Layer architecture

# .Net Technologies/Technique/Tools used/ Project Description

-xUnit for unit testing. MOQ for mocking
-Unit testing Approach used => Arrange, Act and Assert AAA
-Repository patter used for database persistency
-entityframework migrations used for easy database migrations and snapshots
-Nlog used for error logging
-asynchronous calls for multithreading and concurrency
-n-layer architecture used to structiure/architect project and also codebase reuse (seperations of concern) {i used 3 layers Presentation =ODDESTODDS.web, business logic =ODDESTODDS.core, database layer=ODDESTODDSDataAccess Layer
-Asp.net core Dependency injection used to inject factory services into DI container 
-Signal R was used to broadcast notifications to all connected clients which in this case a punker window open with real life changes of games odd.
-Asp.net core MV was used for front end development

# How to run Project

-Data base setup {express or developer version of MSSQl should be install, change configuration strings in appsettings.json to your local Db credentials}
-required tools {visual studion 2017 or higher with asp.net core 2.2 install
-environment setup {run application and restore all nugget packages succesfully, run migrations using update-database  or dotnet ef database update
-if sucessfull, database and table will be created with recent migrations snapshots and settings
-run application to confirm no erros.

#Application URL/Navigaton
-GIG-POC-1 = "/punter/odds"  {football odds are fetch and display in tabular format.

-GIG-POC-2, GIG-POC-3 , GIG-POC4, GIG-POC-5 ="/OddsHandler/CurrentOdds" {this landing screen consist of navigations to confirm the specified feature item}

#Note
to confirm realtime notification of odds updates, maintain to browser and try to have "/punter/odds" open in one window and either "/OddsHandler/CreateOdds" to create odds or go to "/OddsHandler/CurrentOdds" to delete or update and observe signal r send updates to connected clients


# Possible Enhancements & Recommendations
-converting project to SOA (restful services)
-microservice/docerkerization



