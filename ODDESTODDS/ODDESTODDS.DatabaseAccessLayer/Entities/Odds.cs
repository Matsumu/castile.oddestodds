﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ODDESTODDS.DatabaseAccessLayer.Entities
{
   public class Odds:BaseEntity
    {
        public string oddsDetails { get; set; }
        public string home { get; set; }
        public string draw { get; set; }
        public string away { get; set; }

        //other odd types can further be added i.e over 1.5, 2.5 etc




    }
}
