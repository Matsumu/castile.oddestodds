﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ODDESTODDS.DatabaseAccessLayer.Migrations
{
    public partial class initial_Migrations : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Odds",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    dateCreated = table.Column<DateTime>(nullable: false),
                    lastModified = table.Column<DateTime>(nullable: false),
                    isActive = table.Column<bool>(nullable: false),
                    oddsDetails = table.Column<string>(nullable: true),
                    home = table.Column<string>(nullable: true),
                    draw = table.Column<string>(nullable: true),
                    away = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Odds", x => x.id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Odds");
        }
    }
}
