﻿using ODDESTODDS.DatabaseAccessLayer.Dbcontext;
using ODDESTODDS.DatabaseAccessLayer.Entities;
using ODDESTODDS.DatabaseAccessLayer.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Linq;

namespace ODDESTODDS.DatabaseAccessLayer.Repositories
{
    /// <summary>
    /// Repository Implementation for Odds Repository
    /// </summary>
    public class OddsRepository: IOddsRepository
    {
        private ODDESTODDSDbContext _dbContext;
        public OddsRepository(ODDESTODDSDbContext dbContect)
        {
            _dbContext = dbContect;
        }

        public async Task<Odds> create(Odds entity)
        {
            var response = new Odds();
            try
            {
                if (entity!=null)
                {
                    var result = await _dbContext.Odds.AddAsync(entity);
                    if (await _dbContext.SaveChangesAsync()>0)
                    {
                        response = entity;
                        return response;
                    }

                }
            }
            catch (Exception ex)
            {

                throw ex;
            }

            return response;
        }

        public async Task<bool> delete(int id)
        {
            var objectToDelete = new Odds();
            var response = false;
            try
            {
                objectToDelete = await _dbContext.Odds.FindAsync(id);
                if (objectToDelete!=null)
                {
                    _dbContext.Odds.Remove(objectToDelete);
                    if (await _dbContext.SaveChangesAsync()>0)//checking if there is a record affected
                    {
                        response = true;
                        return response;
                    }
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }

            return false;
        }

        public IEnumerable<Odds> fetchAll(int count=50)
        {
            var response = new List<Odds>();
            try
            {
                var result = (from x in _dbContext.Odds select x).Take(count).ToList();
                if (result!=null)
                {
                    response = result;
                    return response;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }

            return response;
        }

        public async Task<Odds> getById(int id)
        {
            var response = new Odds();
            try
            {
                var result = await _dbContext.Odds.FindAsync(id);
                if (result!=null)
                {
                    response = result;
                    return response;

                }

            }
            catch (Exception ex)
            {

                throw ex;
            }

            return response;
        }

        public async Task<Odds> update(int id, Odds entity)
        {
            var response = new Odds();
            try
            {
                var result = _dbContext.Odds.Update(entity);
                if (await _dbContext.SaveChangesAsync()>0)
                {
                    var res = await _dbContext.Odds.FindAsync(id);
                    if (res!=null)
                    {
                        response = res;
                        return response;
                    }
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }


            return response;
        }
    }
}
