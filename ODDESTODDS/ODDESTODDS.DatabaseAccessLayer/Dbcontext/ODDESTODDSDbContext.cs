﻿using Microsoft.EntityFrameworkCore;
using ODDESTODDS.DatabaseAccessLayer.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace ODDESTODDS.DatabaseAccessLayer.Dbcontext
{
    public class ODDESTODDSDbContext:DbContext
    {
        public ODDESTODDSDbContext(DbContextOptions<ODDESTODDSDbContext> options)
                 : base(options)
        {
            Database.Migrate();
        }

        public DbSet<Odds> Odds { get; set; }

    }
}
