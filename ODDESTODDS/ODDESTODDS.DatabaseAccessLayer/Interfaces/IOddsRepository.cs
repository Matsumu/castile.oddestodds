﻿using ODDESTODDS.DatabaseAccessLayer.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ODDESTODDS.DatabaseAccessLayer.Interfaces
{
   public interface IOddsRepository
    {

        IEnumerable<Odds> fetchAll(int count=50);

        Task<Odds> getById(int id);

        Task<Odds> create(Odds entity);

        Task<Odds> update(int id, Odds entity);

        Task<bool> delete(int id);
    }
}
