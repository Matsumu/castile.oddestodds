﻿using ODDESTODDS.core.Enums;
using ODDESTODDS.core.Interfaces;
using ODDESTODDS.core.Requests;
using ODDESTODDS.core.responses;
using ODDESTODDS.DatabaseAccessLayer.Entities;
using ODDESTODDS.DatabaseAccessLayer.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ODDESTODDS.core.Services
{
    /// <summary>
    /// Odds Factory Implementation service
    /// </summary>
    public class OddsFactoryService : IOddsFactory
    {
        private IOddsRepository _repo;

        public OddsFactoryService(IOddsRepository repo)
        {
            _repo = repo;

        }
        /// <summary>
        /// Create/Persist New odds to the database
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
        public async Task<OddsResponse> createOddsAsync(createOddsRequest req)
        {
            var oddsEntity = new Odds();
            var response = new OddsResponse();
            try
            {
                if (req!=null)
                {
                    oddsEntity.away = req.away;
                    oddsEntity.home = req.home;
                    oddsEntity.draw = req.draw;
                    oddsEntity.oddsDetails = req.oddsDetails;

                    var result = await _repo.create(oddsEntity);
                    if (result!=null)
                    {
                        oddsEntity = result;
                        response.response.id = result.id;
                        response.response.home = result.home;
                        response.response.away = result.away;
                        response.response.draw = result.draw;

                        response.StatusCode = StatusCodes.Successfull.ToString();
                        response.StatusMessage = "SuccessFull";

                        return response;
                    }
                    else
                    {
                        response.StatusCode = StatusCodes.Failed.ToString();
                        response.StatusMessage = "UnSuccessFull";

                        return response;
                    }

                }
            }
            catch (Exception ex)
            {

                throw ex;
            }

            return response;
        }
        /// <summary>
        /// detelet odd enty by Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<bool> deleteAsync(int id)
        {
            var response = false;
            try
            {
                response = await _repo.delete(id);
            }
            catch (Exception ex)
            {

                throw ex;
            }

            return response;
        }

        public async Task<FetchOddsResponse> fetchOddsAsync(int count = 50)
        {
            var response = new FetchOddsResponse();
            try
            {
                var result =  _repo.fetchAll(count);
                if (result!=null)
                {
                    foreach (var item in result)
                    {
                        response.response.Add(new BaseResponse { id = item.id, away = item.away, draw = item.draw, home = item.home, oddsDetails = item.oddsDetails });
                    }

                    response.StatusCode = StatusCodes.Successfull.ToString();
                    response.StatusMessage = "SuccessFull";
                    return response;
                }

                else
                {
                    response.StatusCode = StatusCodes.Failed.ToString();
                    response.StatusMessage = "UnSuccessFull";

                }
            }
            catch (Exception ex)
            {

                throw ex;
            }

            return response;
        }

        public async Task<getOddsResponse> getOddAsync(int id)
        {
            var response = new getOddsResponse();
            try
            {
                var result = await _repo.getById(id);
                if (result != null)
                {
                    response.response.id = result.id;
                    response.response.home = result.home;
                    response.response.away = result.away;
                    response.response.draw = result.draw;
                    response.response.oddsDetails = result.oddsDetails;
                    response.StatusCode = StatusCodes.Successfull.ToString();
                    response.StatusMessage = "SuccessFull";
                    return response;
                }

                else
                {
                    response.StatusCode = StatusCodes.Failed.ToString();
                    response.StatusMessage = "UnSuccessFull";

                }
            }
            catch (Exception ex)
            {

                throw ex;
            }

            return response;
        }

        /// <summary>
        /// update database with fresh odd details from Odds Handler
        /// </summary>
        /// <param name="id"></param>
        /// <param name="req"></param>
        /// <returns></returns>
        public async Task<OddsResponse> updateOddsAsync(int id, updateOddsRequest req)
        {
            var oddsEntity = new Odds();
            var response = new OddsResponse();
            try
            {
                if (id>0 && req!=null)
                {
                    oddsEntity.away = req.away;
                    oddsEntity.home = req.home;
                    oddsEntity.draw = req.draw;
                    oddsEntity.oddsDetails = req.oddsDetails;
                    var result = await _repo.update(id, oddsEntity);
                    if (result != null)
                    {
                        response.response.id = result.id;
                        response.response.home = result.home;
                        response.response.away = result.away;
                        response.response.draw = result.draw;
                        response.StatusCode = StatusCodes.Successfull.ToString();
                        response.StatusMessage = "SuccessFull";

                        return response;
                    }
                    else
                    {
                        response.StatusCode = StatusCodes.Failed.ToString();
                        response.StatusMessage = "UnSuccessFull";

                        return response;
                    }

                }

            }
            catch (Exception ex)
            {

                throw ex;
            }

            return response;
        }
    }
}
