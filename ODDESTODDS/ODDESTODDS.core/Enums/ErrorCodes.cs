﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ODDESTODDS.core.Enums
{
   public enum ErrorCodes
    {
        NotFound404=404,
        UnAuthorized401= 401,
        Forbidden403=403,
        BadRequest400=400,
        RequestTimeout408=408,
        InternalServerError500=500,
        NotImplemented501=501,
        BadGateway502=502,
        ServiceUnAvailable503=503
    }

    public enum StatusCodes
    {
       Failed=01,
       Successfull=00,

    }

   
}
