﻿using ODDESTODDS.core.Requests;
using ODDESTODDS.core.responses;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ODDESTODDS.core.Interfaces
{
   public interface IOddsFactory
    {
        Task<OddsResponse> createOddsAsync(createOddsRequest request);
        Task<OddsResponse> updateOddsAsync(int id, updateOddsRequest request);
        Task<getOddsResponse> getOddAsync(int id);
        Task<FetchOddsResponse> fetchOddsAsync(int count=50);
        Task<bool> deleteAsync(int id);




    }
}
