﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ODDESTODDS.core.responses
{
   public class BaseResponse
    {
        public int id { get; set; }
        public string oddsDetails { get; set; }
        public string home { get; set; }
        public string draw { get; set; }
        public string away { get; set; }
     
    }
}
