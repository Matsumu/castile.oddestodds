﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ODDESTODDS.core.responses
{
   public class UpdateOddsResponse
    {
        public BaseResponse response { get; set; } = new BaseResponse();
        public string StatusCode { get; set; }
        public string StatusMessage { get; set; }

    }
}
