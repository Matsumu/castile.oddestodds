﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ODDESTODDS.core.responses
{
   public class FetchOddsResponse
    {
        public List<BaseResponse> response { get; set; } = new List<BaseResponse>();
        public string StatusCode { get; set; }
        public string StatusMessage { get; set; }

    }
}
