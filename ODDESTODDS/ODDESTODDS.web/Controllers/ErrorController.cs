﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using ODDESTODDS.core.Enums;

namespace ODDESTODDS.web.Controllers
{
    public class ErrorController : Controller
    {
        private readonly ILogger<ErrorController> _logger;

        public ErrorController(ILogger<ErrorController> logger)
        {

            _logger = logger;
        }

        [Route("Error/404")]
        public IActionResult Error404()
        {
            _logger.LogInformation("A 404 Error Has occured");
            return View();
        }

        [Route("Error/{code:int}")]
        public IActionResult Error(int code)
        {
            try
            {
                if (code == (int)ErrorCodes.UnAuthorized401)//change code to enumerations
                {
                    ViewData["ErrorMessage"] = "Am sorry, you are not authorize to view this Module, Kindly Contact your Administrator";
                    ViewData["ErrorCode"] = "401 Unauthorized";
                    _logger.LogInformation("401 Unauthorized error Has occured");

                }

                else if (code == (int)ErrorCodes.Forbidden403)
                {
                    ViewData["ErrorMessage"] = "Forbiden Request, please make sure you have access to this path and try again";
                    ViewData["ErrorCode"] = "403 Forbidden";
                    _logger.LogInformation("403 Forbidden error Has occured");
                }



                else if (code == (int)ErrorCodes.BadRequest400)
                {
                    ViewData["ErrorMessage"] = "UnAuthenticated request, Please Authenticate to get the requested response";
                    ViewData["ErrorCode"] = "400 Bad Request";
                    _logger.LogInformation("400 Bad Request error Has occured");
                }



                else if (code == (int)ErrorCodes.RequestTimeout408)
                {
                    ViewData["ErrorMessage"] = "Request Timeout, please try again and if it still persist, contact our administrator";
                    ViewData["ErrorCode"] = "408 Request Timeout";
                    _logger.LogInformation("408 Request Timeout error Has occured");
                }


                else if (code == (int)ErrorCodes.InternalServerError500)
                {
                    ViewData["ErrorMessage"] = "This is so Embarrasing; please try again and contact our administrator if this Persits";
                    ViewData["ErrorCode"] = "500 Internal Server Error";
                    _logger.LogInformation("500 Internal server error Has occured");
                }


                else if (code == (int)ErrorCodes.NotImplemented501)
                {
                    ViewData["ErrorMessage"] = "The request method is not supported by the server and cannot be handled";
                    ViewData["ErrorCode"] = "501 Not Implemented";
                    _logger.LogInformation("501 Not Implemented error Has occured");
                }
                else if (code == (int)ErrorCodes.BadGateway502)
                {
                    ViewData["ErrorMessage"] = "invalid response from gateway server";
                    ViewData["ErrorCode"] = "502 Bad Gateway";
                    _logger.LogInformation("502 Bad Gateway error Has occured");
                }
                else if (code == (int)ErrorCodes.ServiceUnAvailable503)
                {
                    ViewData["ErrorMessage"] = "We are Getting serious Traffic today, The server is not ready to handle the request.Please try again after some minutes";
                    ViewData["ErrorCode"] = "503 Service Unavailable";
                    _logger.LogInformation("503 Service Unavailable error Has occured");
                }


                else
                {
                    ViewData["ErrorMessage"] = "Something is not right at the moment, Please hold while our technical team is fixing it";

                    ViewData["ErrorCode"] = "oops Something Broke";
                    _logger.LogInformation("An Unknown Error Has occured; with error code::" + code);
                }



            }
            catch (Exception ex)
            {
                _logger.Log(LogLevel.Error, "an error has occurred", ex);

            }
            return View();
        }
    }
}