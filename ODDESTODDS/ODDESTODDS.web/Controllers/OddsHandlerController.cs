﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using ODDESTODDS.core.Enums;
using ODDESTODDS.core.Interfaces;
using ODDESTODDS.core.Requests;
using ODDESTODDS.core.responses;
using ODDESTODDS.web.ViewModels;

namespace ODDESTODDS.web.Controllers
{
    public class OddsHandlerController : Controller
    {

        private readonly ILogger<OddsHandlerController> _logger;
        private readonly IOddsFactory _oddService;

        public OddsHandlerController(ILogger<OddsHandlerController> logger, IOddsFactory oddService)
        {
            _logger = logger;
            _oddService = oddService;
        }

        /// <summary>
        /// create Odds => POST
        /// </summary>
        /// <param name="vm"></param>
        /// <returns></returns>
        public async Task<IActionResult> CreateOdds(CreateOddsViewModel vm)
        {
            ViewData["Status"] = string.Empty;
            try
            {
                var response = new OddsResponse();
                if (ModelState.IsValid)
                {
                    //TODO: code can be enhance to use automapper for DTO Mappings in a situation where implementation is larger than this
                    var result = await _oddService.createOddsAsync(new createOddsRequest { oddsDetails = vm.oddsDetails, away = vm.away, draw = vm.draw, home = vm.home });
                    response = result;
                    if (response.StatusCode == StatusCodes.Successfull.ToString())
                    {
                        ViewData["Status"] = response.StatusCode;
                    }

                    else
                    {
                        ViewData["Status"] = response.StatusCode;
                    }
                }
            }
            catch (Exception ex)
            {

                _logger.Log(LogLevel.Error, "An Error has Occured", ex);
                StatusCode(500, ex.Message);

            }


            return View();
        }


        /// <summary>
        /// Odd handler create Odds=>GET
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> CreateOdds()
        {

            return View();
        }

        [HttpPost]
        public async Task<IActionResult> updateodds(UpdateOddsViewModel vm, int Id)
        {
            var viewmodel = new UpdateOddsViewModel();
            ViewData["Status"] = string.Empty;
            try
            {
                var response = new OddsResponse();
                if (ModelState.IsValid && Id > 0)
                {
                    //TODO: code can be enhance to use automapper for DTO Mappings in a situation where implementation is larger than this
                    var result = await _oddService.updateOddsAsync(Id, new updateOddsRequest { oddsDetails = vm.oddsDetails, away = vm.away, draw = vm.draw, home = vm.home });
                    response = result;
                    if (response.StatusCode == StatusCodes.Successfull.ToString())
                    {
                        ViewData["Status"] = response.StatusCode;
                        viewmodel.away = response.response.away;
                        viewmodel.home = response.response.home;
                        viewmodel.draw = response.response.draw;
                        viewmodel.oddsDetails = response.response.oddsDetails;

                        return RedirectToAction("CurrentOdds");

                    }

                    else
                    {
                        ViewData["Status"] = response.StatusCode;
                    }
                }
            }
            catch (Exception ex)
            {

                _logger.Log(LogLevel.Error, "An Error has Occured", ex);
                StatusCode(500, ex.Message);

            }

            return RedirectToAction("CurrentOdds");

           // return View(viewmodel);
        }

        /// <summary>
        /// Get odd details based on routed id for update/view
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> updateodds(int Id = 0)
        {
            var viewmodel = new UpdateOddsViewModel();
            try
            {
                if (Id > 0)
                {
                    var response = await _oddService.getOddAsync(Id);
                    viewmodel.away = response.response.away;
                    viewmodel.home = response.response.home;
                    viewmodel.draw = response.response.draw;
                    viewmodel.id = response.response.id;
                    viewmodel.oddsDetails = response.response.oddsDetails;

                }
            }
            catch (Exception ex)
            {
                _logger.Log(LogLevel.Error, "An Error has Occured", ex);
                StatusCode(500, ex.Message);
            }
            return View(viewmodel);
        }

        /// <summary>
        /// fetches all available odds on the system
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> CurrentOdds()
        {
            var viewModel =new List<FetchCurrentOddsViewModel>();
            try
            {
                var result = await _oddService.fetchOddsAsync();
                if (result.response!=null && result.StatusCode== StatusCodes.Successfull.ToString())
                {
                    foreach (var item in result.response)
                    {
                        viewModel.Add(new FetchCurrentOddsViewModel { Id = item.id, away = item.away, draw = item.draw, home = item.home, oddsDetails = item.oddsDetails });
                    }
                }

            }
            catch (Exception ex)
            {

                _logger.Log(LogLevel.Error, "An Error has Occured", ex);
                StatusCode(500, ex.Message);
            }

            return View(viewModel);
        }


        /// <summary>
        /// Delete  odd by id if found
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> deleteOdd(int id = 0)
        {
            try
            {
                if (id > 0)
                {
                    var result = await _oddService.deleteAsync(id);
                    if (result)
                    {
                        return RedirectToAction("CurrentOdds");
                    }
                }
            }
            catch (Exception ex)
            {

                _logger.Log(LogLevel.Error, "An Error has Occured", ex);
                StatusCode(500, ex.Message);
            }

            return RedirectToAction("CurrentOdds");
        }

    }
}
