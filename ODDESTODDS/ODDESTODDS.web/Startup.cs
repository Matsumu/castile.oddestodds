﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using ODDESTODDS.core.Interfaces;
using ODDESTODDS.core.Services;
using ODDESTODDS.DatabaseAccessLayer.Dbcontext;
using ODDESTODDS.DatabaseAccessLayer.Interfaces;
using ODDESTODDS.DatabaseAccessLayer.Repositories;
using ODDESTODDS.web.signalR;

namespace ODDESTODDS.web
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            var cxn = Configuration["DbconnectionString"].ToString();
            services.AddDbContext<ODDESTODDSDbContext>(options =>
            options.UseSqlServer(cxn)
            .UseLazyLoadingProxies());//cibcontext

            services.AddTransient<IOddsRepository, OddsRepository>();
            services.AddTransient<IOddsFactory, OddsFactoryService>();

            services.AddMvc();
            services.AddSignalR();

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseBrowserLink();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                app.UseStatusCodePagesWithReExecute("/Error/{0}");
            }

            app.UseStaticFiles();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });

            app.UseSignalR(routes =>
            {
                routes.MapHub<OddsHub>("/oddsUpdates");
            });
        }
    }
}
