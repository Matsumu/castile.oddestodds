﻿using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ODDESTODDS.web.signalR
{
    public class OddsHub:Hub
    {
      
       
            public async Task Send(string message)
            {
                await Clients.All.SendAsync("update", message);
         
        }
    }
}
