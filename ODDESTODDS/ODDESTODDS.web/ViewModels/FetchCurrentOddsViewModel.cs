﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ODDESTODDS.web.ViewModels
{
    public class FetchCurrentOddsViewModel
    {
        [Display(Name = "Odd Details")]
        [Required(AllowEmptyStrings = false)]
        public string oddsDetails { get; set; }
        [Display(Name = "Home Odds")]
        [Required(AllowEmptyStrings = false)]
        public string home { get; set; }

        [Display(Name = "Draw Odds")]
        [Required(AllowEmptyStrings = false)]
        public string draw { get; set; }

        [Display(Name = "Away Odds")]
        [Required(AllowEmptyStrings = false)]
        public string away { get; set; }
        public int Id { get; set; } 
    }
}
